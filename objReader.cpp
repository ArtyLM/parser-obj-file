#include "objReader.h"

objReader::objReader(ifstream &Object3D)
    :Object3D(Object3D)
{
    if(!this->Object3D.is_open()) //si le fichier n'est pas ouvert, on stoppe l'�xecution du code
    {
        cout << "Impossible d'ouvrir le fichier!" << endl;
    }
}

objReader::~objReader()
{
    this->Object3D.close(); //fermeture du fichier
}

bool objReader::compare(string str)
{
    bool equivalent = false;
    string content;

    while(this->Object3D)
    {
        getline(this->Object3D, content);
        string extract = content.substr(0, 2);
        if(extract != str)
        {
            this->Object3D.ignore(numeric_limits<streamsize>::max(), '\n');
        }
        else
        {
            cout << content << endl;
        }
    }
    this->Object3D.clear();
    this->Object3D.seekg(0, this->Object3D.beg);

    return equivalent;
}

void objReader::display()
{
    string content;
    while(this->Object3D)
    {
        getline(this->Object3D, content);
        string extract = content.substr(0, 2);
        cout << content << endl;
    }

    this->Object3D.clear();
    this->Object3D.seekg(0, this->Object3D.beg);
}

void objReader::app()
{
    int answer;
    cout << "Que voulez-vous voir dans ce fichier?" << endl;
    cout << "[1] Tout afficher" << endl;
    cout << "[2] Afficher les vertex" << endl;
    cout << "[3] Afficher les vertex de texture" << endl;
    cout << "[4] Afficher les normals" << endl;
    cout << "[5] Afficher les facettes" << endl;

    cin >> answer;

    switch(answer)
    {
        case 1:
            this->display();
            this->app();
            break;
        case 2:
            this->compare("v ");
            this->app();
            break;
        case 3:
            this->compare("vt");
            this->app();
            break;
        case 4:
            this->compare("vn");
            this->app();
            break;
        case 5:
            this->compare("f ");
            this->app();
            break;
        default:
            cout << "Erreur de saisie, veuillez recommencer" << endl;
            this->app();
    }
}


