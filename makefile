C++=g++
CFLAGS=-g++ -Wall
EXEC=main

all: $(EXEC)

main: main.o objReader.o
	$(C++) $(CFLAGS) -o main *.o

main.o: main.cpp
	$(C++) $(CFLAGS) -c main.cpp

objReader.o: objReader.cpp
	$(C++) $(CFLAGS) -c objReader.cpp

clean:
	rm -rf *.o

mrproper: clean
	rm -rf $(EXEC)

