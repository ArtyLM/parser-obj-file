#ifndef OBJREADER_H
#define OBJREADER_H

#include <iostream>
#include <fstream>
#include <string>
#include <limits>

using namespace std;

class objReader
{
    public:
        objReader(ifstream&);
        ~objReader();

        bool compare(string);
        void app();
        void display();

    private:
        ifstream &Object3D;

};

#endif // OBJREADER_H
